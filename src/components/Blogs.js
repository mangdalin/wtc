import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import { useState, useEffect } from 'react';


export default function Blogs() {
  const [isLoading, setIsLoading] = useState(true);
  const [loadedMeetups, setLoadedMeetups] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      'https://api-wefarm-default-rtdb.firebaseio.com/blog.json'
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const meetups = [];

        for (const key in data) {
          const meetup = {
            id: key,
            ...data[key]
          };

          meetups.push(meetup);
        }
        setIsLoading(false);
        setLoadedMeetups(meetups);
      });
  }, []);
  const cardBlog = loadedMeetups.map(item => {
    return (
      <CardItem
              key={item.id}
              src={item.image}
              text={item.title}
              subtext={item.description}              
              path={item.path}
      />
    )
  })
  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }  
  return (
    <div className='cards'>
      <h1>Blogs</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
             {cardBlog}
          </ul>
        </div>
      </div>
    </div>
  );  
}


