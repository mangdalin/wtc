import React from 'react';
import { useState, useEffect } from 'react';
import HeroImageItems from './HeroImageItems';

export default function HeroImages() {

    const [isLoading, setIsLoading] = useState(true);
  const [loadedMeetups, setLoadedMeetups] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      'https://api-wefarm-default-rtdb.firebaseio.com/heroImage.json'
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const meetups = [];

        for (const key in data) {
          const meetup = {
            id: key,
            ...data[key]
          };

          meetups.push(meetup);
        }
        setIsLoading(false);
        setLoadedMeetups(meetups);
      });
  }, []);
  const images = loadedMeetups.map(item => {
    return (
        <HeroImageItems key={item.id} image={item.image} />
    )
  })
  if (isLoading) {
    return (
      <div className='container'>
        <div className='text-center'>
          <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
        
      </div>
    );
  }  
  return (
    <div className="container">
    <section>
        <div className="row" id="base">
            {images}
        </div>
        <hr></hr>
    </section>  
    </div>
  );
    
}
