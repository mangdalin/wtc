import React from "react"

export default function ActivityImageItems(props){
    console.log(props.image)
    return(
        <div className="carousel-item active">
              <img 
              src={props.image}
              className="d-block w-100" alt={props.id} />
        </div>
    )
}