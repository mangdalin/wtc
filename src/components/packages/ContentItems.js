import React from "react"
import "./ContentItems.css"

import SlideBar from "./SlideBar"

import ActivityImage from "./ActivityImage"

export default function ContentItems(props){
    return(
            <div className="container">
            <div className="wrapper mt-4">
                
                {/* content */}
            <div className="w-50">
                    <h2>What you'll do</h2>       
                    <ActivityImage title=" Lorem ipsum dolor sit amet, consectetur"/>

                    <ActivityImage title="Lorem ipsum dolor sit amet, consectetur"/>

                    <ActivityImage title="Lorem ipsum dolor sit amet, consectetur"/>
                </div>

                <SlideBar title={props.title} to={"/booking"}/>
                
            </div>
            <br></br>  
            <div className="container">
            
            </div>    
        </div>  
    )
}