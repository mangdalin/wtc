import React from "react"
import { Button } from "../Button"

export default function SlideBar(props){
    return(
        <>
            <form className="rm_border">
                    <div className="form-header">
                        <h2>{props.title}</h2>
                        <h5>30$ per person</h5>
                        <h6>Available Now</h6>
                        </div>
                        <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <span className="form-label">Check In</span>
                                <input className="form-control" type="date" required />
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <span className="form-label">Check Out</span>
                                <input className="form-control" type="date" required />
                            </div>
                        </div>
                        </div>
                        <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <span className="form-label">Adults</span>
                                <select className="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <span className="select-arrow"></span>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <span className="form-label">Children</span>
                                <select className="form-control">
                                    <option>0</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <span className="select-arrow"></span>
                            </div>
                        </div>
                        </div>
                        <div className="text-center mt-4">   
                        <Button buttonStyle='btn--outline1' linkTo={props.to}>Booking</Button>
                        </div>       
                    </form> 
        
        </>
    )
}