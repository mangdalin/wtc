import React from "react"
import "./Contact.css"

export default function Contact(){
    return(
        <>
        <div className="container">
            <div className="row">
                <div className="col-sm-6  text-dark p-3">
                <h1 className="font-weight-light mt-2">Quick Contact</h1>
                    <form className="mt-4">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="form-group mt-2">
                                    <input className="form-control" type="text" placeholder="name" />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="form-group mt-2">
                                    <input className="form-control" type="email" placeholder="email address" />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="form-group mt-2">
                                    <input className="form-control" type="text" placeholder="phone" />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="form-group mt-2">
                                    <textarea className="form-control" rows="3" placeholder="message"/>
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <button type="submit" className="btn btn-dark mt-4"><span> SUBMIT</span></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-sm-6 text-dark p-3">
                <h1 className="font-weight-light mt-2">Location</h1>
                    {/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
                    <iframe className="mt-4" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1414.5432570108064!2d103.11326973514869!3d13.033522031143812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310537d91b629569%3A0x75b3b34e93774574!2sSoun%20Panha%20library%20and%20cafe!5e0!3m2!1sen!2skh!4v1643789738661!5m2!1sen!2skh"
                    width="100%" 
                    height="76%" 
                    frameBorder={0} style={{border: 0}}>
      </iframe>
                </div>
            </div>
        </div>
        
        </>
        
        
    )
}