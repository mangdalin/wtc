import React from "react"
import "./Booking.css"


export default function Booking(props){
    return(
        <div className="container">                   

            <div className="row">
                <div className="col-lg-7">
                <div className="row w-75 fw-bold border mt-4">

                <p className="h3 mt-4 mb-4 fw-bold">Your Trip</p>
                
                <div className="col-lg-6 fs-6">
                    <label htmlFor="checkIn" className="form-label">Check in</label>
                    <input type="date" className="form-control" id="checkIn" value="2022-01-21" placeholder="Example John Cena" />   
                            
                </div>
                <div className="col-lg-6 fs-6">
                    <label htmlFor="checkOut" className="form-label">Check out</label>
                    <input type="date" className="form-control" id="checkOut" value="2022-01-24" placeholder="000-000-000" />
                </div>

                <div className="col-lg-6 fs-6">
                <label htmlFor="Adults" className="form-label">Adults</label>
                <select class="form-select" aria-label="Default select example">                                        
                    <option selected value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                </div>
                <div className="col-lg-6 fs-6">
                <label htmlFor="baby" className="form-label">Baby</label>
                <select class="form-select" aria-label="Default select example">                                        
                    <option selected value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>                                            
                </div>
                 

                <p className="h3 mt-4 mb-4 fw-bold">Your information</p>
                
                <div className="col-lg-6 fs-6">
                    <label htmlFor="fullName" className="form-label">Full name *</label>
                    <input type="text" className="form-control" id="fullName" placeholder="Example John Cena" />                    
                </div>
                <div className="col-lg-6 fs-6">
                    <label htmlFor="phoneNumber" className="form-label">Phone number *</label>
                    <input type="text" className="form-control" id="phoneNumber" placeholder="000-000-000" />
                </div>
                <div className="col-lg-6 fs-6">
                    <label htmlFor="address" className="form-label">Address</label>
                    <input type="text" className="form-control" id="address" placeholder="Enter your address" />                    
                </div>
                <div className="col-lg-6 fs-6">
                    <label htmlFor="email" className="form-label-">Email</label>
                    <input type="text" className="form-control" id="email" placeholder="Enter your Email" />
                </div>

                {/* Payment */}


                <p className="h3 mt-4 mb-4 fw-bold">Payment</p>

                <div className="row">                    
                    <div className="col-lg-4 border">                        
                        <div className="form-check fs-6">
                            <input className="form-check-input mt-2" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                            <label className="form-check-label ms-2 text-uppercase " htmlFor="flexRadioDefault1">                            
                            <img src="https://marketplace.magento.com/media/catalog/product/0/d/0daa_extensionicons_240x240_1.jpg"
                                    alt="payway-logo" width={"40"} height={"auto"}/>
                                    &nbsp; payway 
                            </label>
                        </div>                        
                    </div>

                    <div className="col-lg-4 border ms-5">                        
                        <div className="form-check fs-6">
                            <input className="form-check-input mt-2" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                            <label className="form-check-label ms-2  align-items-center text-uppercase" htmlFor="flexRadioDefault1">                            
                            <img src="https://play-lh.googleusercontent.com/QxYqXysf12cI6QQyGAG9hX0pqa9GrnWiy_67xqxbnFxtVj5ILt09Vlqzi8_hpyAQEn8"
                                    alt="payway-logo" width={"40"} height={"auto"}/>
                                    &nbsp; Wing
                            </label>
                        </div>                        
                    </div>

                    <div className="mt-5"> 
                    </div>
                    <button type="button" className="btn btn-dark float-start mb-5" data-bs-toggle="modal" data-bs-target="#modalForm">
                        Pay now
                        </button>
                        {/* Modal */}
                        <div className="modal fade" id="modalForm" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">Payway ABA BANK (Fake)</h5>
                                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"/>
                                    </div>
                                    <div className="modal-body">                                
                                        <img src="https://images.pexels.com/photos/11084560/pexels-photo-11084560.png" 
                                        className="img-fluid rounded-start" alt="..." />                                                                                                       
                                    </div>
                                    <div className="text-center mb-5">
                                        <a href="/images/receipt.png" download>
                                            <button type="button" class="btn btn-dark">Download ticket</button>                                        
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>                                
                </div>                                 
            </div>
                </div>
            

                {/* slide bar */}

            <div className="col-lg-5 booking-slide-bar">
                <div className="row w-100 fw-bold mt-4">
                        <div className="card mb-3">
                            <div className="row mb-4">
                                <div className="col-lg-4 mt-4">
                                    <img src="https://images.pexels.com/photos/2749481/pexels-photo-2749481.jpeg" className="img-fluid rounded-start" alt="..." />
                                </div>
                                <div className="col-md-8">
                                    <div className="card-body">
                                    <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <p className="card-text"><small className="text-muted">Battambang, Cambodia</small></p>
                                </div>
                            </div>
                            <hr className="mt-3" />
                            <div className="row">
                                <div className="col-lg-10">
                                    <p className="h5 fw-bold">Price detail</p>
                                    <div className="row">
                                        <p className="h6 ">$30 x 1 Adult</p>
                                    </div>
                                    <div className="row">
                                        <p className="h6 ">Insurance</p>
                                    </div>                                
                                </div>
                                <div className="col-lg-2">
                                    <p className="h5 fw-bold">&nbsp;</p>
                                    <div className="row">
                                        <p className="h6 ">$30</p>
                                    </div>
                                    <div className="row">
                                        <p className="h6 ">$5</p>
                                    </div>                                 
                                </div>
                                <hr className="mt-3" />
                                <div className="col-lg-10">
                                    <p className="h5 fw-bold">Total</p>   
                                </div>
                                <div className="col-lg-2">
                                    <p className="h5 fw-bold">$35</p>   
                                </div>                       
                            </div>                
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

        
        </div>
    )
};
