import React from 'react'
export default function HeroImageItems(props) {
    return (
        <div className="col-md-5 mt-4 col-lg-4">
            <img src={props.image}className="img-fluid" alt={props.id} />                    
        </div> 
    )
}
