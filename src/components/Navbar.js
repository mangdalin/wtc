import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import { Button } from './Button';


function Navbar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);




  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <Link to='/home' className='navbar-logo' onClick={closeMobileMenu}>
          Wefarm
          </Link>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <Link to='/home' className='nav-links' onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/our-packages' className='nav-links' onClick={closeMobileMenu}>
                Our packages
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/blogs' className='nav-links' onClick={closeMobileMenu}>
                Blogs
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/about' className='nav-links' onClick={closeMobileMenu}>
                About us
              </Link>
            </li>
 
          </ul>
          {<Button buttonStyle='btn--outline' linkTo={"/sign-up"}>SIGN UP</Button>}
          {<Button buttonStyle='btn--outline' linkTo={"/sign-in"}>SIGN IN</Button>}
        </div>
      </nav>
    </>
  );
}

export default Navbar;
