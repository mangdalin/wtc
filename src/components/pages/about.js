import React from 'react';
import '../../App.css';
import Footer from '../Footer';
import Contact from '../packages/Contact';



export default function About() {
  return (
    <>
    <div className='container'>
        <div className='row text-center' >
           <p className='font-weight-bold h3 mt-5 fw-bold'>About us</p>
            <div className='w-100'>
              <p className='text-start'>
              WeFarm has been founding by a group of young people from Battambang province in 2020 who had been involving with the Mekong Youth Farm Network (Y-Farm). 
              We have been inspiring young people from different perspective and region to share their insight and stories. When we come back together were discussing
               the problems that happen in our communities and ask ourselves how can we be part of the solutions. Why there are no young people interested in farming, 
               what will happen if, in the futcdure, there are no farmers anymore? We do not have answers at that time, we have been involving with the workshop, talking to people. In January-September 2020, we have got a chance to pilot a project of our three programs, under support from Gret Organization. Together with 11 young volunteers and 30 young people joining the farm camp, plus a survey, we finally figure out three programs under WeFarm to move forward.
              </p>
            </div>
        </div>  
    </div>
    <Contact />
    <Footer />
    </>
  )
}
