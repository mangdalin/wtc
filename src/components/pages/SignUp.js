import React from 'react';
import "./SignUp.css"

import { Button } from "../Button"

export default function SignUp() {
  return (
    <div className="text-center m-5-auto">
            <h2>Join us</h2>
            <h5>Create your personal account</h5>
            <form action="/home">
                <p>
                    <label>Email address</label><br/>
                    <input type="email" name="email" required />
                </p>
                <p>
                    <label>Password</label><br/>
                    <input type="password" name="password" required />
                </p>
                <p>
                    <label>Repeat Password</label><br/>
                    <input type="password" name="password" required />
                </p>

                <Button buttonStyle='btn--outline1' linkTo={"/sign-in"}>SIGN UP</Button>
                 
            </form>

        </div>
  );
}
