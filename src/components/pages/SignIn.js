import React from 'react';
import { Link } from 'react-router-dom'
import { Button } from '../Button';

export default function SignIn() {
  return (
    <div className="text-center m-5-auto">
        <h2>Sign in to us</h2>
        <div className='mx-1 mx-md-4'>
            <form action="/home">
                <p>
                    <label>Username or email address</label><br/>
                    <input type="text" name="first_name" required />
                </p>
                <p>
                    <label>Password</label>
                    <br/>
                    <input type="password" name="password" required />
                </p>
                <Button buttonStyle='btn--outline1' linkTo={"/home"}>SIGN IN</Button>
    
            </form>
        </div>
        <footer>
            <p>First time? <Link to="/sign-up">Create an account</Link>.</p>
        </footer>
    </div>
  )
}
