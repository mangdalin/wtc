import React from 'react';
import '../../App.css';
import Blogs from '../Blogs'
import Footer from '../Footer'

export default function blogs() {
  return(
    <>
      <Blogs />
      <Footer />
    </>
  )
}
