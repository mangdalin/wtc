import React from 'react';
import '../../App.css';
import HeroImages from '../packages/HeroImages';
import ContentItems from '../packages/ContentItems';
import Contact from '../packages/Contact'
import Footer from '../Footer'

export default function farmTourAndStay() {
  return (
    <>
      <div className='container'>
        <h1 className='text-start h1 mt-4'>Farm Tour and stay</h1>
      </div>
      <HeroImages />  
      <ContentItems title="Farm Tour" />
      <Contact />
      <Footer />
    </>
  )
}
