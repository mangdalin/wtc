import React from 'react';
import '../../App.css';

import HeroImages from '../packages/HeroImages';
import ContentItems from '../packages/ContentItems';
import Contact from '../packages/Contact'
import Footer from '../Footer'

export default function FarmEduAndCamp() {
  return (
    <>
      <div className='container'>
        <h1 className='text-start h1 mt-4'>Farm education and camp</h1>
      </div>

      <HeroImages />  
      <ContentItems title="Farm EDU" />
      <Contact />
      <Footer />
    </>
  )
}
