import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import { useState, useEffect } from 'react';


function Cards() {
  const [isLoading, setIsLoading] = useState(true);
  const [loadedMeetups, setLoadedMeetups] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      'https://api-wefarm-default-rtdb.firebaseio.com/card.json'
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const meetups = [];

        for (const key in data) {
          const meetup = {
            id: key,
            ...data[key]
          };

          meetups.push(meetup);
        }
        setIsLoading(false);
        setLoadedMeetups(meetups);
      });
  }, []);
  const cardPackage = loadedMeetups.map(item => {
    return (
      <CardItem
              key={item.id}
              src={item.image}
              text={item.title}
              subtext={item.description}              
              path={item.path}
      />
    )
  })
  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }  
  return (
    <div className='cards'>
      <h1>Our Packages</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
             {cardPackage}
          </ul>
        </div>
      </div>
    </div>
  );  

}

export default Cards;
