import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import FarmEduAndCamp from './components/pages/farm-edu-and-camp';
import FarmSchool from './components/pages/farm-school';
import farmTourAndStay from './components/pages/farm-tour-and-stay';
import OurPackages from './components/pages/our-packages';
import Blogs from './components/pages/blogs';
import About from './components/pages/about';
import SignUp from './components/pages/SignUp';
import SignIn from './components/pages/SignIn';
import Booking from './components/packages/Booking';
import EventDetails from './components/pages/EventDetail';
function App() {
  return (
    <>
      <Router>
        <Navbar navstatus="false"/>
        <Switch>
          <Route path='/home' component={Home} />
          <Route path='/' exact component={SignUp} />
          <Route path='/sign-in' component={SignIn} />
          <Route path='/farm-school' component={FarmSchool} />
          <Route path='/farm-tour-and-stay' component={farmTourAndStay} />
          <Route path='/farm-edu-and-camp' component={FarmEduAndCamp} />
          <Route path='/our-packages' component={OurPackages} />
          <Route path='/blogs' component={Blogs} />
          <Route path='/events-detail' component={EventDetails} />
          <Route path='/blogs-detail' component={EventDetails} />
          <Route path='/about' component={About} />
          <Route path='/booking' component={Booking} />
          <Route path='/sign-up' component={SignUp} />
          <Route path='/sign-in' component={SignIn} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
